# This is a repo of a few of our utilities

You will most likely need to create the fixrs network:

```
docker network create fixrs
```

That's it

# Traffic routing

Whenever a project has this label in their docker-compose.yml file :

```
labels:
  - traefik.http.routers.${PROJECT_NAME}.rule=Host(`${PROJECT_BASE_URL}`)
```

The project should have .env file defining those values. 
If we take buddha station, you will find

```
PROJECT_NAME=one_buddha
PROJECT_BASE_URL=one_buddha.localhost
```

So whenever you call http://one_buddha.localhost , it will point to your traefik from this repo.
Traefik will then match the rule from docker-compose and send the trafic to this container.
It's a bit like black magic.
